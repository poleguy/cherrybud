# cherrybud
cherrytree viewer

Working on this issue:
https://github.com/giuspen/cherrytree/issues/328

This currently can read simple ctd files (xml format only), e.g.

http://www.poleguy.com/simple.ctd

All it does at the moment is render the name and the tree correctly.

immediate todo:
render text in another view
clean up gui (open file button)
create package file for google play etc.
open files based on extension

long term:
allow editing
handle detecting and auto loading changes over the network
